package com.acquitygroup.webtest.model.account

import spock.lang.Specification


class GenderTest extends Specification {

    def "get gender by mnenomic"() {

        expect:
        Gender.getGender(mnemonic) == genderObject

        where:
        mnemonic | genderObject
        'm'      | Gender.MALE
        'M'      | Gender.MALE
        'f'      | Gender.FEMALE
        'F'      | Gender.FEMALE
        'n'      | Gender.UNKNOWN
        ''       | Gender.UNKNOWN
        null     | Gender.UNKNOWN
    }

    def "test constructors and values"() {
        expect:
        value == genderObject.getValue()

        where:
        value | genderObject
        1     | Gender.MALE
        2     | Gender.FEMALE
        3     | Gender.UNKNOWN
    }
}
