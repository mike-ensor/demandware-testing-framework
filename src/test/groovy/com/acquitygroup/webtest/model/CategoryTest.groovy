package com.acquitygroup.webtest.model

import com.acquitygroup.webtest.model.category.CategoryFactory
import spock.lang.Specification
import spock.lang.Unroll

class CategoryTest extends Specification {

    @Unroll
    def "Convert Category to Query String"() {

        expect:
        instance.getQueryString() == queryString

        where:
        instance                                                                        | queryString
        CategoryFactory.getCategory(CategoryType.QUERY_PARAM, "category", "Some Title") | "?cgid=category"
    }

    @Unroll
    def "Convert Category to URL String"() {

        expect:
        instance.getQueryString() == queryString

        where:
        instance                                                           | queryString
        CategoryFactory.getCategory(CategoryType.URL, "category", "Title") | "/category"
    }

}
