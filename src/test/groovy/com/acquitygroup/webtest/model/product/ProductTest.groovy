package com.acquitygroup.webtest.model.product

import spock.lang.Specification
import spock.lang.Unroll

class ProductTest extends Specification {

    @Unroll
    def "Convert Product to Query String for ProductDetailsPage"() {

        expect:
        instance.build().getQueryString() == queryString

        where:
        instance                                                                                                                                                                                              | queryString
        Product.builder(new ProductIdentifier("12416789", "12416789"), "Title of Product").color(new ProductColor("Navy", "GYX"))                                                                             | "?pid=12416789&dwvar_12416789_color=GYX"
        Product.builder(new ProductIdentifier("12416789", "12416789"), "Title of Product").size(new ProductSize("32")).color(new ProductColor("Navy", "GYX"))                                                 | "?pid=12416789&dwvar_12416789_color=GYX&dwvar_12416789_size=32"
        Product.builder(new ProductIdentifier("12416789", "12416789"), "Title of Product").color(new ProductColor("Navy", "GYX")).size(new ProductSize("32")).categoryIdentifier("my-category-name")          | "?pid=12416789&dwvar_12416789_color=GYX&dwvar_12416789_size=32#cgid=my-category-name"
        Product.builder(new ProductIdentifier("12416789", "12416789"), "Title of Product").color(new ProductColor("Navy", "GYX")).start(1).size(new ProductSize("32")).categoryIdentifier("my-category-name") | "?pid=12416789&dwvar_12416789_color=GYX&dwvar_12416789_size=32#cgid=my-category-name&start=1"
        Product.builder(new ProductIdentifier("12416789", "12416789"), "Title of Product").size(new ProductSize("32"))                                                                                        | "?pid=12416789&dwvar_12416789_size=32"

    }
}
