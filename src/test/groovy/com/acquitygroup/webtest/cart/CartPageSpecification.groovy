package com.acquitygroup.webtest.cart

import com.acquitygroup.webtest.model.product.Product
import com.acquitygroup.webtest.model.product.ProductColor
import com.acquitygroup.webtest.model.product.ProductIdentifier
import com.acquitygroup.webtest.model.product.ProductPrice
import com.acquitygroup.webtest.page.cart.CartPage
import com.acquitygroup.webtest.page.checkout.CheckoutShippingPage
import com.acquitygroup.webtest.page.myaccount.LogoutPage
import com.acquitygroup.webtest.util.CheckoutUtil
import geb.Browser
import geb.spock.GebReportingSpec
import spock.lang.Ignore

class CartPageSpecification extends GebReportingSpec {

    private static final String FIVE_OFF_COUPON = "FIVEOFF"

    protected Product product
    protected Integer productQuantity = 3

    public void setupTemplate(Browser browser) {
        browser.to LogoutPage
    }

    def setup() {
        setup:
        setupTemplate(browser)

        product = Product.builder(new ProductIdentifier("682875540326-1", "682875540326-1"), "Checked Silk Tie")
                .color(new ProductColor("Citrine"))
                .categoryIdentifier("mens-accessories-ties")
                .price(new ProductPrice(29.99, 29.99))
                .start(4)
                .build()
    }

    def "remove a product"() {
        when: "go to product detail page, at product to cart"
        CheckoutUtil.addProductToCart(getBrowser(), 1, product)

        then: "user is at the cart page"
        to CartPage

        and: "verify product is in the cart on the cart page"
        productTitle(0) == product.getTitle()
        productAttribute(0, "Color") == product.getColor().getDisplay()

        when: "click remove item link"
        removeItemLink(0).click()

        then: "back to the cart page"
        at CartPage

        and: "verify cart is empty"
        isCartEmpty() == true
    }

    def "add a product to the cart an verify on the cart page"() {
        when: "go to product detail page, at product to cart"
        CheckoutUtil.addProductToCart(getBrowser(), productQuantity, product)

        then: "user is at the cart page"
        to CartPage

        and: "verify product is in the cart on the cart page"
        productTitle(0) == product.getTitle()
        productAttribute(0, "Color") == product.getColor().getDisplay()

        when: "user goes to checkout as guest"
        checkoutAsGuest.click()

        then: "user is at checkout shipping page"
        at CheckoutShippingPage
    }

    @Ignore("Incorporate OCAPI to determine coupon for cart")
    def "user can add coupon"() {
        given:
        at CartPage

        when:
        String couponCode = FIVE_OFF_COUPON
        couponCodeInput = couponCode
        couponSubmit.click(CartPage)

        then:
        couponNumber(0).text() == FIVE_OFF_COUPON
        couponAction(0).text() == "Applied"

        and: "verify order total is deducted"
        BigDecimal calculatedSubTotal = product.getPrice().getSalesPrice() * productQuantity
        BigDecimal discount = 5.00
        cartOrderTotalDiscount.text() == "- \$${discount}"
        cartOrderTotalSubtotal.text() == "\$${calculatedSubTotal}"
        cartOrderTotalShipping.text() == "N/A"
        cartOrderTotalSalesTax.text() == "N/A"
        cartOrderTotal.text() == "\$${calculatedSubTotal - discount}"

    }

    @Ignore("Incorporate OCAPI to determine coupon for cart")
    def "user can remove coupon"() {
        given:
        at CartPage

        when:
        String couponCode = FIVE_OFF_COUPON
        couponCodeInput = couponCode
        couponSubmit.click(CartPage)

        then:
        couponNumber(0).text() == FIVE_OFF_COUPON
        couponAction(0).text() == "Not Applied"

        and: "Remove coupon"
        couponRemove(0).click(CartPage)
        couponNumber(0).isDisplayed() == false
    }
}
