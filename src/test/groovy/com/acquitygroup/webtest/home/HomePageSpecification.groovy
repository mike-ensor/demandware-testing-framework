package com.acquitygroup.webtest.home

import com.acquitygroup.webtest.page.home.HomePage
import com.clickconcepts.geb.OverridableGebReportingSpec

class HomePageSpecification extends OverridableGebReportingSpec {

    def "user can view the home page"() {

        given: "user goes home page"
        to HomePage

        expect: "user is at homepage"
        at HomePage

    }
}
