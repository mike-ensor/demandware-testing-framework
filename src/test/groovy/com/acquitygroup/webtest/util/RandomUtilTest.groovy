package com.acquitygroup.webtest.util

import spock.lang.Specification
import spock.lang.Unroll

class RandomUtilTest extends Specification {

    @Unroll
    def "make sure that the random number is greater than zero and less than the max since Max is not inclusive"() {

        given: "Random Utility requesting a max number"
        def result = RandomUtil.getInt(requetedMax)

        expect: "random result is less than max and greater than or equal to zero"
        result < max

        and: "ensure result is greater than equal to 0"
        result >= 0

        where:
        requetedMax | max
        1           | 1
        512         | 512
        2000        | 2000
    }
}
