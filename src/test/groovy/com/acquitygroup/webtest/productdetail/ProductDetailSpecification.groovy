package com.acquitygroup.webtest.productdetail

import com.acquitygroup.webtest.model.product.Product
import com.acquitygroup.webtest.model.product.ProductColor
import com.acquitygroup.webtest.model.product.ProductIdentifier
import com.acquitygroup.webtest.model.product.ProductPrice
import com.acquitygroup.webtest.module.product.ProductDetailsModule
import com.acquitygroup.webtest.page.myaccount.LogoutPage
import com.acquitygroup.webtest.page.product.ProductDetailsPage
import com.acquitygroup.webtest.service.RemoteProductService
import geb.Browser
import geb.spock.GebReportingSpec
import spock.lang.Shared

class ProductDetailSpecification extends GebReportingSpec {

    @Shared
    protected Product product
    @Shared
    protected RemoteProductService productService

    /**
     * Method allowing for setup() method to be overridden
     * @param browser
     */
    public void setupTemplate(Browser browser) {
        browser.to LogoutPage
    }

    def setupSpec() {
        URI config = getClass().getResource('/OCapiTestConfig.groovy').toURI()

        productService = new RemoteProductService(config)
        product = productService.getProductById("682875540326-1")
    }

    /**
     *  Run before each feature.  Cannot be overridden by child classes
     */
    def setup() {
        setup:
        setupTemplate(browser)
    }

    def "display product on product details page"() {

        when: "going to the Product Details page with a Silken Tie"
        toProductPageWithDefaultParameters(product)

        then: "expect to be at the product details page"
        at ProductDetailsPage

        and: "ensure product tile is available"
        productDetails.productTitle.text() == product.getTitle()
        verifyCurrentColor(product.getColor().getDisplay())
    }


    def "add a product to the cart"() {
        def addQuantity = 1
        Integer quantityBeforeAddToCart

        given: "go to product detail page"
        toProductPageWithDefaultParameters(product)

        expect: "user is at product detail page"
        at ProductDetailsPage

        when: "add product to cart"
        quantityBeforeAddToCart = productDetails.miniCart.getCurrentProductCount(0)
        productDetails.addProductToCart(addQuantity)

        then: "ensure product is in the cart"
        waitFor { productDetails.miniCart.isProductInCart(product) }

        and: "make sure that the mini-cart product quantity and title are correct"
        productDetails.miniCart.productTitle(0) == product.getTitle()
        productDetails.miniCart.getCurrentProductCount(0) == quantityBeforeAddToCart + addQuantity
    }

    def "display and select color by default"() {

        product = Product.builder(new ProductIdentifier("25752235", "25752235"), "Checked Silk Tie")
                .categoryIdentifier("mens-accessories-ties")
                .price(new ProductPrice(29.99, 29.99))
                .build()

        given: "goto the default product details page without a color selected"
        toProductPageWithDefaultParameters(product)

        expect: "No color swatch has been selected"
        verifyCurrentColor()

        when: "Click the Citrine color"
        product = Product.builder(new ProductIdentifier("25752235", "25752235"), "Checked Silk Tie")
                .color(new ProductColor("Citrine", "Citrine"))
                .categoryIdentifier("mens-accessories-ties")
                .price(new ProductPrice(29.99, 29.99))
                .build()

        productDetails.setSwatch("Select Color", product.getColor().getDisplay(), ProductDetailsModule.SwatchState.ON)

        then: "user is at product detail page"
        at ProductDetailsPage
        verifyCurrentColor(product.getColor().getDisplay())
    }

    /**
     * Because the price might change, the test will only verify that the MSRP price is greater than the sale price
     */
    def "verify that the MSRP and the sale price are both available"() {
        given: "goto the product details page"
        toProductPageWithDefaultParameters(product)

        expect: "expect to be at the product details page"
        at ProductDetailsPage

        and: "Retail Price is greater than the sale price"
        productDetails.retailPrice.text() > productDetails.salePrice.text()
    }

    /**
     *  example URLs for the large and small images
     *      /on/demandware.static/Sites-SiteGenesis-Site/Sites-apparel-catalog/default/v1352480129614/images/large/PG.949612424S.COBATSI.PZ.jpg
     *      /on/demandware.static/Sites-SiteGenesis-Site/Sites-apparel-catalog/default/v1352480129614/images/small/PG.949612424S.COBATSI.PZ.jpg
     */
    def "verify that the product does have an image and at least one alternative view"() {
        given: "go to product detail page"
        toProductPageWithDefaultParameters(product)

        expect:
        productDetails.primaryImage.attr("title").startsWith product.getTitle()
        productDetails.primaryImage.attr("src").contains("images/large")

        and: "corresponding selected thumbnail URL (by convention) is the same URL as the large image, except for the 'small' in the path"
        productDetails.selectedProductThumbnail.attr("src").contains("images/small")
    }

    private void verifyCurrentColor(String expectedColor = "") {
        productDetails.selectedSwatchColor(expectedColor).text() == product.getColor()
    }

    private void toProductPageWithDefaultParameters(Product initProduct) {
        to ProductDetailsPage, initProduct
    }

}
