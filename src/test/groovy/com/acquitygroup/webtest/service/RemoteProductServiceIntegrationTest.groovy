package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.model.product.Product
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

class RemoteProductServiceIntegrationTest extends Specification {

    private static final Logger LOG = LoggerFactory.getLogger(RemoteProductServiceIntegrationTest.class)

    private RemoteProductService service

    def setup() {
        URI config = getClass().getResource('/OCapiTestConfig.groovy').toURI()
        service = new RemoteProductService(config)
        service != null
    }

    def "test getProductsInCategory"() {
        when:
        List<Product> productDocuments = service.getAvailableProductsInCategory("mens-accessories-ties")

        then:
        productDocuments != null
        productDocuments.size() > 0
    }

    def "get single product"() {
        when:
        Product product = service.getProductById("682875540326-1")

        then:
        product != null
        product.getPid().getPid() == "682875540326-1"
        product.getColor().getDisplay() == "Citrine"
        LOG.debug(product.getQueryString())
    }

    def "set color for product"() {
        when:
        Product product = service.getProductById("682875719029-1")

        then:
        product != null
        product.getColor().getDisplay() == "Canary"
    }

}
