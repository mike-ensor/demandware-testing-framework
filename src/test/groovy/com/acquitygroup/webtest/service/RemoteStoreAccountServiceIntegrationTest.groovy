package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.model.account.Gender
import com.acquitygroup.webtest.model.account.RegisteredAccount
import com.acquitygroup.webtest.model.account.AccountProfile
import com.acquitygroup.webtest.util.RandomUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

class RemoteStoreAccountServiceIntegrationTest extends Specification {

    private static final Logger LOG = LoggerFactory.getLogger(RemoteStoreAccountServiceIntegrationTest.class)

    private RemoteStoreAccountService service

    def setup() {
        URI config = getClass().getResource('/OCapiTestConfig.groovy').toURI()
        service = new RemoteStoreAccountService(config)
        service != null
    }

    def "register customer on storefront"() {
        given:
        String first = "First"
        String last = "Last"
        String email = "${first}.${last}_" + RandomUtil.getInt(10000) + "@mailinator.com".toLowerCase()

        AccountProfile customer = new AccountProfile(first, last, email, "p@\$\$w0rd1!", Gender.MALE)

        when:
        RegisteredAccount account = service.registerCustomer(customer)

        then:
        account.getCustomerNumber() != null
        account.getProfile().getFirst() == first
        LOG.debug("Created Customer ID: {}", account.getCustomerNumber())
    }
}
