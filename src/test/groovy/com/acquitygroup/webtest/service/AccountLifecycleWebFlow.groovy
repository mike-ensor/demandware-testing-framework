package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.model.account.Gender
import com.acquitygroup.webtest.model.account.RegisteredAccount
import com.acquitygroup.webtest.model.account.AccountProfile
import com.acquitygroup.webtest.util.RandomUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
class AccountLifecycleWebFlow extends Specification {

    private static final Logger LOG = LoggerFactory.getLogger(AccountLifecycleWebFlow.class)
    private static final String CUSTOMER_EMAIL = "person.email_" + RandomUtil.getInt(1000) + "@mailinator.com"

    @Shared
    RemoteAccountService accountService
    @Shared
    RemoteStoreAccountService storeAccountService

    // Yes, this violates testing philosophies with dependencies between features, but this is intended to run via @Stepwise
    @Shared
    RegisteredAccount foundAccount

    def setupSpec() {
        URI config = getClass().getResource('/OCapiTestConfig.groovy').toURI()

        // StoreAccount is only for storefront account creation
        storeAccountService = new RemoteStoreAccountService(config)
        accountService = new RemoteAccountService(config)
    }

    def "search for customer"() {
        when:
        List<RegisteredAccount> results = accountService.search(CUSTOMER_EMAIL)

        then:
        results.isEmpty()
    }

    def "register customer"() {
        String first = "First"
        String last = "Last"

        AccountProfile unregisteredAccount = new AccountProfile(first, last, CUSTOMER_EMAIL, "p@\$\$w0rd1!", Gender.MALE)

        when:
        foundAccount = storeAccountService.registerCustomer(unregisteredAccount)

        then:
        foundAccount != RegisteredAccount.INVALID
    }

    def "search for newly created customer"() {
        when: "search for the same customer"
        List<RegisteredAccount> accounts = accountService.search(CUSTOMER_EMAIL)

        then: "verify customer was found"
        accounts.size() > 0
        accounts.get(0).getCustomerNumber() == foundAccount.getCustomerNumber()
    }

    def "remove newly created customer"() {
        when: "search for created customer"
        List<RegisteredAccount> accounts = accountService.search(CUSTOMER_EMAIL)
        accounts.size() == 1
        RegisteredAccount account = accounts.get(0)

        and:
        accountService.removeAccount(account.getCustomerNumber())

        then: "verify customer is removed"
        !accountService.isActiveCustomerAccount(account.getCustomerNumber())
    }
}