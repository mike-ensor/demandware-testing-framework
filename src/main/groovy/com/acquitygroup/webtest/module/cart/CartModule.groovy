package com.acquitygroup.webtest.module.cart

import com.acquitygroup.webtest.model.product.Product

class CartModule extends AbstractCartModule {

    static content = {
        subTotal {}
        productTitle {}
        productQuantity {}
        productSalePrice {}
        productLineItemTitle {}
    }

    @Override
    boolean isProductInCart(Product product) {
        return false
    }
}
