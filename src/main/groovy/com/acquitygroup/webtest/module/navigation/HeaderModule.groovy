package com.acquitygroup.webtest.module.navigation

import geb.Module

/**
 * This module is intended to contain the common header elements contained most pages within the Site Genesis project
 */
class HeaderModule extends Module {

    static content = {
        /**
         * Link to register for an account
         */
        registerLink { $("a.user-register") }
        /**
         * Link to Login
         */
        login { $("a.login") }
		/**
		 * targets the full name of the logged in user
		 *
		 * <b>Required: False to suppress non-logged in users</b>
		 */
		loggedInName (required: false){$("div.custinfo a.user-account")}
    }
}
