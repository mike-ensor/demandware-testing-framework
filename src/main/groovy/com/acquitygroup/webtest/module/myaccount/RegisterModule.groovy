package com.acquitygroup.webtest.module.myaccount

import com.acquitygroup.webtest.model.account.Customer
import geb.Module

class RegisterModule extends Module {

    static content = {

        primaryHeader(wait: true) { $("#primary h1", 0) }

        registerHere(required: false) { $("#register-here") }
        registerFacebook(required: false) { $("#register-facebook") }

        firstName { $("#dwfrm_profile_customer_firstname") }
        lastName { $("#dwfrm_profile_customer_lastname") }

        email { $("#dwfrm_profile_customer_email") }
        confirmEmail { $("#dwfrm_profile_customer_emailconfirm") }
        password { $("#dwfrm_profile_login_password") }
        confirmPassword { $("#dwfrm_profile_login_passwordconfirm") }

        addToEmailList { $("#dwfrm_profile_customer_addtoemaillist") }

        submit { $("button", type: "submit", name: "dwfrm_profile_confirm") }
    }

    public void fillForm(Customer customer) {
        firstName << customer.getFirstName()
        lastName << customer.getLastName()
        email = ""
        email << customer.getEmail()
        confirmEmail = ""
        confirmEmail << customer.getEmail()
        password << customer.getPassword()
        confirmPassword << customer.getPassword()

        addToEmailList.click()
    }

}
