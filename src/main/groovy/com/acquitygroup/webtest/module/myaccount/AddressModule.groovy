package com.acquitygroup.webtest.module.myaccount

import org.openqa.selenium.StaleElementReferenceException;

import com.acquitygroup.webtest.model.Address
import geb.Module

class AddressModule extends Module {

    static content = {

        addressForm(required: false) { $("#edit-address-form") }
        
		firstName(required: false) { $("input", id: iEndsWith("_firstName")) }
		lastName(required: false) { $("input", id: iEndsWith("_lastName")) }
		address1(required: false) { $("input", id: iEndsWith("_address1")) }
		address2(required: false) { $("input", id: iEndsWith("_address2")) }
		country(required: false) { $("select", id: iEndsWith("_country")) }
		state(required: false) { $("select", id: iEndsWith("_states_state")) }
		city(required: false) { $("input", id: iEndsWith("_city")) }
		zip(required: false) { $("input", id: iEndsWith("_zip")) }
		phone(required: false) { $("input", id: iEndsWith("_phone")) }

        
		formError (required: false) { $("span.error-message") }
		requiredFieldError (required: false) { $("span.error") }
		
		savedAddresses(required: false){ $("select", id: iEndsWith("_addressList"))}
		savedAddress(required: false){int index -> savedAddresses.find("option", index)}
    }

    /**
     * @deprecated Use fillAddressForm({@link Address} instead
     * @param addressIDIn
     * @param firstNameIn
     * @param lastNameIn
     * @param address1In
     * @param address2In
     * @param countryIn
     * @param stateIn
     * @param cityIn
     * @param zipIn
     * @param phoneIn
     * @return
     */
    @Deprecated
    def fillAddressFormAndSubmit(String addressIDIn, String firstNameIn, String lastNameIn,
                                 String address1In, String address2In, String countryIn, String stateIn,
                                 String cityIn, String zipIn, String phoneIn) {

        addressID << addressIDIn
        firstName << firstNameIn
        lastName << lastNameIn
        address1 << address1In
        address2 << address2In
        country = countryIn
        state = stateIn
        city << cityIn
        zip << zipIn
        phone << phoneIn

        applyButton.click()
        //addressForm.submit()
    }

    /**
     * Fills in an address on the MyAccount and Registration pages
     * @param address Address
     */
    def fillAddressForm(Address address) {
		emptyAddressForm()
        //addressID << address.getAddressName()
        firstName << address.getFirstName()
        lastName << address.getLastName()
        address1 << address.getAddress1()
        address2 << address.getAddress2()
        country = address.getCountryCode()
        state = address.getStateCode()
        city << address.getCity()
        zip << address.getZipCode()
        phone << address.getPhone()
    }

    def emptyAddressForm() {
        //addressID = ""
		int count = 0;
		while(count < 4){
		try{
        firstName = ""
        lastName = ""
        address1 = ""
        address2 = ""

        city = ""
        zip = ""
        phone = ""
		count = count+4;
		}
		catch(StaleElementReferenceException e){
			count++;
			refreshElements()
		}
		}
		
    }
	
	def refreshElements(){
		
		addressForm { $("#edit-address-form") }
		
		firstName { $("input", id: iEndsWith("_firstName")) }
		lastName { $("input", id: iEndsWith("_lastName")) }
		address1 { $("input", id: iEndsWith("_address1")) }
		address2 { $("input", id: iEndsWith("_address2")) }
		country { $("select", id: iEndsWith("_country")) }
		state { $("select", id: iEndsWith("_states_state")) }
		city { $("input", id: iEndsWith("_city")) }
		zip { $("input", id: iEndsWith("_zip")) }
		phone { $("input", id: iEndsWith("_phone")) }
	}

}
