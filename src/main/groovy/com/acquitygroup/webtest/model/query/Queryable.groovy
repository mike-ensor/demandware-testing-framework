package com.acquitygroup.webtest.model.query

public interface Queryable {

    /**
     * Returns the string responsible beyond the base URL and EndPoint
     *
     * <em>Example</em>: ?param1=value1&param2=value2
     * <em>Or</em>: /param1/value1/param2/value2
     *
     * @return String
     */
    String getQueryString()

}