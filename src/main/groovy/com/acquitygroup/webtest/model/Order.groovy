package com.acquitygroup.webtest.model
/**
 * Model class encapsulating what an Order is in a default Demandware site
 */
class Order {
    private final Integer orderNumber
    private final String orderStatus
    private final Date orderDate
    private final List<Shipment> shipments
    private final Address billingAddress

    /**
     * Constructs a full Order
     * @param orderNumber
     * @param orderStatus
     * @param orderDate
     * @param shipments
     * @param billingAddress
     */
    Order(Integer orderNumber, String orderStatus, Date orderDate, List<Shipment> shipments, Address billingAddress) {
        this.orderNumber = orderNumber
        this.orderStatus = orderStatus
        this.orderDate = orderDate
        this.shipments = shipments
        this.billingAddress = billingAddress
    }

    String getOrderNumber() {
        return orderNumber
    }

    String getOrderStatus() {
        return orderStatus
    }

    Date getOrderDate() {
        return orderDate
    }

    List<Shipment> getShipments() {
        return shipments
    }

    Address getBillingAddress() {
        return billingAddress
    }
}
