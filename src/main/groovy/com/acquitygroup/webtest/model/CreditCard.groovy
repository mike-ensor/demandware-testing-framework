package com.acquitygroup.webtest.model

import java.util.List;

/**
 * Model class encapsulating what a Credit Card is in a default Demandware site
 */
class CreditCard {
	private final String name
	private final String type
	private final String number
	private final String expiryMonth
	private final Integer expiryYear
	private final Integer cvv

	/**
	 * Constructs a full Credit Card
	 * @param name
	 * @param type
	 * @param number
	 * @param expiryMonth
	 * @param expiryYear
	 */
	CreditCard(String name, String type, String number, String expiryMonth, Integer expiryYear, Integer cvv) {
		this.name = name
		this.type = type
		this.number = number
		this.expiryMonth = expiryMonth
		this.expiryYear = expiryYear
		this.cvv = cvv
	}

	String getName() {
		return name
	}

	String getType() {
		return type
	}

	String getNumber() {
		return number
	}

	String getExpiryMonth() {
		return expiryMonth
	}

	Integer getExpiryYear() {
		return expiryYear
	}
	
	Integer getCVV() {
		return cvv
	}
}
