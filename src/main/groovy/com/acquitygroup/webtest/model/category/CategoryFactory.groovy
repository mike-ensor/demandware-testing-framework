package com.acquitygroup.webtest.model.category

import com.acquitygroup.webtest.model.CategoryType

class CategoryFactory {

    static CategoryLevel getCategory(CategoryType type, String category, String categoryTitle) {

        CategoryLevel returnCategory

        switch (type) {
            case CategoryType.QUERY_PARAM:
                returnCategory = new QueryParamCategoryLevel(category, categoryTitle)
                break
            case CategoryType.URL:
                returnCategory = new URLCategoryLevel(category, categoryTitle)
                break
        }

        returnCategory
    }
}
