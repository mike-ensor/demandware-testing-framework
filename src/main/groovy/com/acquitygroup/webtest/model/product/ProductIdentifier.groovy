package com.acquitygroup.webtest.model.product

class ProductIdentifier {

    private final String pid
    private final String variantId

    ProductIdentifier(String variantId) {
        this.pid = null
        this.variantId = variantId
    }

    ProductIdentifier(String variantId, String pid) {
        this.variantId = variantId
        this.pid = pid
    }

    String getPid() {
        // TODO: Null Safe Return
        return pid
    }

    String getVariantId() {
        return variantId
    }
}
