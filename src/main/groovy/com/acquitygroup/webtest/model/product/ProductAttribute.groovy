package com.acquitygroup.webtest.model.product

public interface ProductAttribute {

    String getAttributeValue()

    String getKey()

}