package com.acquitygroup.webtest.model.product

class ProductColor implements ProductAttribute {

    private final String display
    private final String label

    ProductColor(String display, String label) {
        this.display = display
        this.label = label
    }

    ProductColor(String key) {
        this.label = key
        this.display = key
    }

    String getDisplay() {
        display
    }

    String getLabel() {
        label
    }

    @Override
    String getAttributeValue() {
        label
    }

    @Override
    String getKey() {
        "color"
    }
}
