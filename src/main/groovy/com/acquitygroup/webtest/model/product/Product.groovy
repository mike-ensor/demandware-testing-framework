package com.acquitygroup.webtest.model.product

import com.acquitygroup.webtest.model.query.ProductRequestParameterQuery
import com.acquitygroup.webtest.model.query.QueryParameterStrategy

class Product extends AbstractProduct {

    private final String categoryUrlSegment
    private final Integer start

    /**
     * @param title
     * @param categoryUrlSegment
     * @param pid
     * @param color
     * @param price
     * @param size
     * @param start
     */
    protected Product(String title, String categoryUrlSegment, ProductIdentifier pid, ProductColor color, ProductPrice price, ProductSize size, Integer start) {
        super(title, color, pid, price, size)
        this.start = start
        this.categoryUrlSegment = categoryUrlSegment
    }

    protected Product(String title, String categoryUrlSegment, ProductIdentifier pid, ProductColor color, ProductPrice price, ProductSize size, QueryParameterStrategy queryStrategy, Integer start) {
        super(title, color, pid, price, size, queryStrategy)
        this.start = start
        this.categoryUrlSegment = categoryUrlSegment
    }

    public String getCategoryUrlSegment() {
        categoryUrlSegment
    }

    public Integer getStart() {
        start
    }

    /**
     * Delegates query parameter creation to {@link QueryParameterStrategy} created during building of product()
     *
     * @return String URL
     */
    @Override
    public String getQueryString() {
        // delegate to query parameter strategy for query string
        getStrategy().getQueryString(this)
    }

    /**
     * Builds a product using the default ProductRequestParameterQuery() strategy
     * @param productIdentifier
     * @param title
     * @return
     */
    public static Builder builder(ProductIdentifier productIdentifier, String title) {
        // Defaults to ProductRequestParameterQuery()
        return new Builder(productIdentifier, title, new ProductRequestParameterQuery())
    }

    /**
     * Builder containing the attributes and identifiers to build out a {@link Product}
     *
     * Extend to add more attributes and features to the Product as needed.  Please take note, you will need to override the {@link Product} method to add functionality
     *
     */
    public static class Builder {
        protected final String title
        protected final ProductIdentifier pid
        protected final QueryParameterStrategy queryStrategy

        protected ProductColor color
        protected ProductPrice price
        protected ProductSize size
        protected String categoryUrlSegment
        protected Integer start

        protected Builder(ProductIdentifier productIdentifier, String title, QueryParameterStrategy queryStrategy) {
            this.queryStrategy = queryStrategy
            this.title = title
            this.pid = productIdentifier
        }

        QueryParameterStrategy getQueryStrategy() {
            queryStrategy
        }

        public Builder color(ProductColor color) {
            this.color = color
            this
        }

        public Builder price(ProductPrice price) {
            this.price = price
            this
        }

        public Builder size(ProductSize size) {
            this.size = size
            this
        }

        public Builder categoryIdentifier(String categoryIdentifier) {
            this.categoryUrlSegment = categoryIdentifier
            this
        }

        public Builder start(int start) {
            this.start = start
            this
        }

        public Product build() {
            new Product(title, categoryUrlSegment, pid, color, price, size, getQueryStrategy(), start)
        }

    }

}
