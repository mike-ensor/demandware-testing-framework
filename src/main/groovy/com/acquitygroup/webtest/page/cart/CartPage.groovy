package com.acquitygroup.webtest.page.cart

import com.acquitygroup.webtest.page.checkout.CheckoutLoginPage
import com.acquitygroup.webtest.page.checkout.CheckoutShippingPage
import geb.Page
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class CartPage extends Page {

    private static final Logger LOG = LoggerFactory.getLogger(CartPage.class)

    static url = "default/Cart-Show"

    static at = {
        title.trim() == "My SiteGenesis Cart"
    }

    static content = {
        cartTable { $("#cart-table") }

        cartEmptyLabel(required: false, cache: false) { $(".cart-empty", 0) }

        productDetailRow { Integer row -> cartTable.find("tr.cart-row", row) }
        productDetailsBlock { Integer index -> productDetailRow(index).find("td.item-details") }
        productTitle { Integer index -> productDetailsBlock(index).find("div.name a").text() }
        productSku { Integer index -> productDetailsBlock(index).find("div.sku span.value").text() }
        // write complicated "attribute" map or similar...span.attributes span.label = key, span.value=value...ex: span.label.text() = "Color" span.value.value = "Blue"
        productAttribute { Integer index, String label -> productDetailsBlock(index).find("div.attribute span.label", text: label + ":").next("span.value").text() }

        // Quantity Details class="item-quantity-details"
        productQuantityDetailsBlock { Integer index -> productDetailRow(index).find("td.item-quantity-details") }
        removeItemLink(to: CartPage) { Integer index -> productQuantityDetailsBlock(index).find("button.button-text", value: "Remove") }
        checkoutAsGuest(to: CheckoutShippingPage) { $("form.cartcontinue button.checkout-as-guest") }
		checkoutButton(to: CheckoutLoginPage){ $("form#checkout-form button") }

        // Coupons
        couponContainer { $("div.cart-coupon-code") }
        couponCodeInput { couponContainer.find("input#dwfrm_cart_couponCode") }
        couponSubmit { couponContainer.find("a.add-coupon-button") }

        couponLineItemRow(required: false) { Integer index -> $("tr.rowcoupons")[index] }
        couponNumber(required: false) { Integer index -> couponLineItemRow(index).find("td.coupon-details div.cartcoupon span.value") }
        couponRemove(required: false) { Integer index -> couponLineItemRow(index).find("td.coupon-actions button[type=submit]") }
        couponAction(required: false) { Integer index -> couponLineItemRow(index).find("td.coupon-actions span.coupon-status") }

        // Cart Totals
        cartOrderTotalContainer { $("div.cart-order-totals") }
        cartOrderTotalsTable { cartOrderTotalContainer.find("table.order-totals-table") }
        cartOrderTotalRow { String rowName -> cartOrderTotalsTable.find("tr.order-${rowName}")[0] }
        cartOrderTotalValue { String rowName -> cartOrderTotalRow(rowName).find("td+td") }

        // Helpers
        cartOrderTotalDiscount { cartOrderTotalRow("discount").find("td+td") }
        cartOrderTotalSubtotal { cartOrderTotalRow("subtotal").find("td+td") }
        cartOrderTotalShipping { cartOrderTotalRow("shipping").find("td+td") }
        cartOrderTotalSalesTax { cartOrderTotalRow("sales-tax").find("td+td") }
        cartOrderTotal { cartOrderTotalRow("total").find("td+td") }

    }

    public boolean isCartEmpty() {
        LOG.debug("Empty Cart Value: {} ", cartEmptyLabel.text())
        boolean found = cartEmptyLabel.text().length() > 0
        found
    }
}
