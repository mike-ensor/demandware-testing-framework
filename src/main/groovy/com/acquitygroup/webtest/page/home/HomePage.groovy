package com.acquitygroup.webtest.page.home

import com.acquitygroup.webtest.module.navigation.HeaderModule
import com.acquitygroup.webtest.module.navigation.NavigationModule
import geb.Page
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Homepage for a Site Genesis Implementation
 */
class HomePage extends Page {

    private static final Logger LOG = LoggerFactory.getLogger(HomePage.class)

    /**
     * Base URL
     */
    static url = "default/Home-Show"

    /**
     * Title for Site Genesis
     */
    static at = {
        LOG.debug("at() in Home Page")
        title.trim() == ("SiteGenesis Online Store")
    }

    /**
     * Content Variables available on the HomePage
     * <ul>
     *      <li>headerModule - Links to the {@link HeaderModule}</li>
     *      <li>navigationModule - Links to the {@link NavigationModule}</li>
     * </ul>
     */
    static content = {
        /**
         * BLAH BLAH
         */
        headerModule { module HeaderModule }
        navigationModule { module NavigationModule }
    }

}
