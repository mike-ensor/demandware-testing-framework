package com.acquitygroup.webtest.page.error

import geb.Page

class MissingPage extends Page {

    static at = {
        header.text() == "Page Not Found"
    }

    static content = {
        header { $("div#primary > h1", 0)}
    }

}

