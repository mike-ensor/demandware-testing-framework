package com.acquitygroup.webtest.page.myaccount

import com.acquitygroup.webtest.model.Address;
import com.acquitygroup.webtest.model.CreditCard;
import geb.Page
import com.acquitygroup.webtest.module.myaccount.AddressModule

class AddressesPage extends Page {

	static url = "default/Address-List";
	
	static at = {
		waitFor {
			title.equals("My SiteGenesis Addresses")
		}
	}
	
	static content = {
		createNewAddress { $("a.address-create") }
		editAddress { $("a.address-edit") }
		deleteAddress { $("a.address-delete") }
		addressID(required: false) { $("input", id: iEndsWith("_addressid")) }
		
		addressModule(required: false) { module AddressModule }
		
		deleteButton(required: false) { $(addressForm).find(".delete-button") }
		applyButton(required: false, wait: true) { $("button", name: "dwfrm_profile_address_create") }
		editButton(required: false){$("button", name: "dwfrm_profile_address_edit")}
		cancelButton(required: false) { $(addressForm).find(".cancel-button") }
		
		miniAddressContainer (required: false, cached: false)	{ $("div.address-list li") }
		miniAddressID (required: false, cached: false)			{ $("div.mini-address-title") }
		miniAddressName (required: false, cached: false)		{ $("div.mini-address-name") }
		miniAddress	(required: false, cached: false)			{ $("div.mini-address-location address") }
		
	}
	
	def deleteAddress(Address address){
		withConfirm(true) { $(".mini-address-title", text: contains(address.getAddressName())).parent().find(".address-delete").click() }
   }
   
   def isAddressAvailable(Address address){
	   $("div.mini-address-location address", text: contains(address.getAddress1()))
   }
   def fillAddressForm(Address address) {
	   addressID = ""
	   addressID << address.getAddressName()
	   addressModule.fillAddressForm(address)
   }

}
