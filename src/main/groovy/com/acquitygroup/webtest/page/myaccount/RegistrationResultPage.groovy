package com.acquitygroup.webtest.page.myaccount

import geb.Page

class RegistrationResultPage extends Page {

    static url = "default/Account-Show"

    static at = {
        // TODO: Link this from the previous page (section 5.8.1 of geb handbook)
        waitFor { title.equals("My SiteGenesis Account Home") }
    }
	
	static content = {
		primaryHeader { String contents -> $("#primary h1", 0).text().contains(contents) }
	}

}
