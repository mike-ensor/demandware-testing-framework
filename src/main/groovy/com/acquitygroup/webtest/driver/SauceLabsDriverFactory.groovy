package com.acquitygroup.webtest.driver

import com.google.common.base.Preconditions
import com.google.common.base.Strings
import com.google.common.collect.Maps
import geb.driver.RemoteDriverOperations
import org.openqa.selenium.Capabilities
import org.openqa.selenium.Platform
import org.openqa.selenium.WebDriver

class SauceLabsDriverFactory {

    WebDriver create(String specification, String user, String accessKey, Map<String, Object> additionalCapabilities = [:]) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(user), "Sauce Labs Username is Required" as Object)
        Preconditions.checkArgument(!Strings.isNullOrEmpty(accessKey), "Sauce Labs API Key/Password is Required" as Object)

        def remoteDriverOperations = new RemoteDriverOperations(getClass().classLoader)
        Class<? extends WebDriver> remoteWebDriverClass = remoteDriverOperations.remoteWebDriverClass
        if (!remoteWebDriverClass) {
            throw new ClassNotFoundException('org.openqa.selenium.remote.RemoteWebDriver needs to be on the classpath to create SauceLabs RemoteWebDriverInstances')
        }

        def url = new URL("http://$user:$accessKey@ondemand.saucelabs.com:80/wd/hub")

        def parts = specification.split(":", 3)
        def name = parts[0]
        def platform = parts.size() > 1 ? parts[1] : null
        def version = parts.size() > 2 ? parts[2] : null

        def browser = remoteDriverOperations.softLoadRemoteDriverClass('DesiredCapabilities')."$name"();
        if (platform) {
            try {
                platform = Platform."${platform.toUpperCase()}"
            } catch (MissingPropertyException ignore) {
            }
            browser.setCapability("platform", platform)
        }
        if (version != null) {
            browser.setCapability("version", version.toString())
        }

        // pass in additional capabilities
        for (String key : additionalCapabilities.keySet()) {
            browser.setCapability(key, additionalCapabilities.get(key) as Object)
        }

        remoteWebDriverClass.getConstructor(URL, Capabilities).newInstance(url, browser)
    }

    public static Closure<WebDriver> createSauceDriver(String sauceBrowser) {
        String username = System.getenv("saucelabs.username") ?: System.getProperty("saucelabs.username")
        // "password" is used instead of API key because CI's (Jenkins and Bamboo) mask "passwords" by default
        String apikey = System.getenv("saucelabs.password") ?: System.getProperty("saucelabs.password")
        String jobName = System.getenv("saucelabs.jobname") ?: System.getProperty("saucelabs.jobname")

        Map<String, Object> additionalChromeCapabilities = Maps.newHashMap()
        additionalChromeCapabilities.put(
                "chrome.switches", Arrays.asList("--disable-extensions", "--incognito")
        )

        if (!Strings.isNullOrEmpty(jobName)) {
            additionalChromeCapabilities.put("job-name", jobName)
        }

        return { new SauceLabsDriverFactory().create(sauceBrowser, username, apikey, additionalChromeCapabilities) }
    }

}
