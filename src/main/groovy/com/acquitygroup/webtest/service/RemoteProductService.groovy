package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.model.product.Product
import com.acquitygroup.webtest.model.product.ProductColor
import com.acquitygroup.webtest.model.product.ProductIdentifier
import com.acquitygroup.webtest.model.product.ProductPrice
import com.google.common.annotations.Beta
import com.google.common.annotations.VisibleForTesting
import com.google.common.collect.ImmutableList
import com.google.common.collect.Maps
import groovyx.net.http.HttpResponseDecorator
import org.apache.http.HttpStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Purpose is to interact with the OCAPI to manipulate products
 *
 * NOTE: This is BETA and should only be used for experimentation.  VERY likely to change implementation
 *
 * TODO: Modify to reflect Master and Variations
 *
 */
@Beta
class RemoteProductService extends AbstractStoreService {

    private static final String DEFAULT_FETCH_COUNT = "10"
    private static final Logger LOG = LoggerFactory.getLogger(RemoteProductService.class)

    @VisibleForTesting
    RemoteProductService(URI configFilePath) {
        super(configFilePath)
    }
/*
    1)      Product Search:
        a.       http://my-store.demandware.net/s/Store-Site-Store/dw/shop/v13_6/product_search?format=json&count=12&start=0&expand=images,prices&refine_1=cgid=catalog-name&sort=brand&refine_2=color=Blue|Red|Tan&client_id=????
        b.      Take the first result:
    2)      Product Link:
        a.       http://my-store.demandware.net/s/Store-Site-Store/dw/shop/v13_6/products/11111?client_id=????&format=json
        b.      Expand this URI for the “images” on the individual product
    3)      Product Images:
        a.       http://my-store.demandware.net/s/Store-Site-Store/dw/shop/v13_6/products/11111/images?client_id=?????&format=json
        b.      Notice the “images” after the product ID in the path (/products/{id}/images)
    */

    public List<Product> getAvailableProductsInCategory(String category) {
        def builder = ImmutableList.builder()

        def query = getDefaultQueryParameterMap() << ["count": DEFAULT_FETCH_COUNT, "start": "0", "expand": "images,prices,availability", "refine_1": "cgid=${category}"]
        def path = "product_search"

        try {
            HttpResponseDecorator resp = client.get(path: path, query: query) as HttpResponseDecorator
            LOG.debug("API::GetProductsInCategory Response {}", resp.getStatus())
            if (resp.getStatus() == HttpStatus.SC_OK) {
                def data = resp.getData()
                if (hasSearchResults(data)) {
                    def productResultSet = data.hits
                    // TODO call template method for extensibility to convert product serach data to product or product sub-type
                    productResultSet.each { hit ->
                        // only include products that are orderable
                        if (hit.orderable == true) {
                            String productId = hit.product_id
                            def product = getProductById(productId)
                            if (product != null) {
                                builder.add(product)
                            }
                        }
                    }
                }
            }
        } catch (ex) {
            // do nothing, no results returned is the ultimate
            LOG.error("Query for Products in Category failed with [{}] response", ex)
            LOG.error(ex.printStackTrace().toString())
        }

        builder.build()
    }

    public Product getProductById(String productId) {
        Map<String, String> data = getProductAttributeMapById(productId)
        Product product = null

        if (data != null && data.size() > 0) {
            product = Product.builder(new ProductIdentifier(productId, productId), data.name)
                    .color(new ProductColor(data.color, data.color))
                    .categoryIdentifier(data.primary_category_id)
                    .price(new ProductPrice(new BigDecimal(data.price_max), new BigDecimal(data.price)))
                    .build()
        }

        product
    }

    private Map<String, String> getProductAttributeMapById(String productId) {
        def query = getDefaultQueryParameterMap() << [
                "expand": "availability,bundled_products,links,promotions,options,images,prices,variations,set_products"
        ]

        def path = "products/${productId}"
        Map<String, String> product = Maps.newHashMap()

        try {
            HttpResponseDecorator resp = client.get(path: path, query: query) as HttpResponseDecorator

            LOG.debug("API::GetProductById Response {}", resp.getStatus())

            if (resp.getStatus() == HttpStatus.SC_OK) {
                def data = resp.getData()
                if (hasProductData(data)) {
                    product.put("brand", (String) data.brand)
                    product.put("id", (String) data.id);
                    product.put("long_description", (String) data.long_description);
                    product.put("name", (String) data.name);
                    product.put("page_description", (String) data.page_description);
                    product.put("page_keywords", (String) data.page_keywords);
                    product.put("page_title", (String) data.page_title);
                    product.put("primary_category_id", (String) data.primary_category_id);
                    product.put("short_description", (String) data.short_description);
                    product.put("master", (data.type != null ? (String) data.type.master : "false"));
                    // inventory
                    product.put("stock_level", (String) data.inventory.stock_level)
                    // pricing
                    product.put("price", (String) data.price)

                    product.put("price_max", (String) (data.price_max != null ? data.price_max : data.price))

                    product.put("color", (String) data.c_color)
                    // images
                }
            }
        } catch (ex) {
            LOG.error("Query for Products in Category failed with [{}] response", ex.response.status)
        }

        product
    }

    private boolean hasSearchResults(data) {
        data.hits != null
    }

    private boolean hasProductData(data) {
        data != null
    }
}
