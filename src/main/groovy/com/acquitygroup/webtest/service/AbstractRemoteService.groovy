package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.rest.CookieAwareRestClientGroovy
import com.google.common.collect.ImmutableMap
import com.google.common.collect.Maps
import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.HttpResponseException
import org.apache.http.HttpStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class AbstractRemoteService {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRemoteService.class)

    protected final CookieAwareRestClientGroovy client
    protected final String baseUrl
    protected final String apiVersion

    AbstractRemoteService() {
        this(getClass().getResource('OCapiConfig.groovy').toURI())
    }

    AbstractRemoteService(URI configFilePath) {
        String configEnvironment = System.getProperty("environment") ?: ""

        def config = new ConfigSlurper(configEnvironment).parse(new File(configFilePath).toURI().toURL())
        baseUrl = config.website.baseUrl

        apiVersion = config.website.version

        client = new CookieAwareRestClientGroovy("${baseUrl}" + getResource() + "/${apiVersion}/")
        client.setHeaders(ImmutableMap.<String, String> of('x-dw-client-id', (String) config.website.clientId))
    }

    public abstract String getResource()

    protected Map<String, String> getDefaultQueryParameterMap() {
        ["format": "json"]
    }

    /**
     * Template method to extract data from post
     * @param path
     * @param query
     * @param bodyMap
     * @return
     */
    protected Map<String, String> doPost(String path, Map<String, String> query, Map<String, Serializable> bodyMap) {
        Map<String, String> data = Maps.newHashMap()

        HttpResponseDecorator resp
        try {
            resp = client.post(path: path, query: query, body: bodyMap, requestContentType: ContentType.JSON) as HttpResponseDecorator
            LOG.debug("API::Response Status: {}", resp.getStatus())
            if (resp.getStatus() == HttpStatus.SC_OK) {
                def rawData = resp.getData()
                if (rawData != null) {
                    data = rawData as Map<String, String>
                }
                LOG.debug(data.toString())
            }
        } catch (HttpResponseException ex) {
            LOG.error("Failed POST: HTTP StatusCode {}; Fault Type: {}; Fault Message {}", ex.response.getStatus(), ex.response.getData().fault.type, ex.response.getData().fault.message)
        }
        data
    }

    protected boolean doDelete(String path, Map<String, String> query) {
        boolean success = false
        try {
            HttpResponseDecorator resp = client.delete(path: path, query: query) as HttpResponseDecorator
            LOG.debug("API::Response Status: {}", resp.getStatus())
            if (resp.getStatus() == HttpStatus.SC_NO_CONTENT) {
                success = true
            }
        } catch (ex) {
            LOG.error("POST failed with [{}] response", ex)
            LOG.debug("Response Data: " + ex.response.getData())
        }

        success
    }
}
