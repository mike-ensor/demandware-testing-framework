package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.model.product.Product
import com.google.common.annotations.VisibleForTesting
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class RemoteBasketService extends AbstractStoreService {

    private static final Logger LOG = LoggerFactory.getLogger(RemoteBasketService.class)

    @VisibleForTesting
    RemoteBasketService(URI configFilePath) {
        super(configFilePath)
        clearBasketSession()
    }

    public void clearBasketSession() {
        client.clearSession()
    }

    public boolean hasBasketSession() {
        client.hasSession()
    }

    public Object addProductToBasket(Product product) {

        def query = getDefaultQueryParameterMap()

        def productObjectMap = [
                "product_id": product.getPid().getPid(),
                "quantity": 1
        ]
        // basket/this/add?format=xml
        def path = "basket/this/add"

        def basket = doPost(path, query, productObjectMap)

        basket
    }
}
