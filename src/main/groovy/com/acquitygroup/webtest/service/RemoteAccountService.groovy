package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.model.account.Gender
import com.acquitygroup.webtest.model.account.RegisteredAccount
import com.acquitygroup.webtest.model.account.AccountProfile
import com.google.common.annotations.VisibleForTesting
import com.google.common.collect.ImmutableList
import groovyx.net.http.HttpResponseDecorator
import org.apache.http.HttpStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class RemoteAccountService extends AbstractDataService {

    private static final Logger LOG = LoggerFactory.getLogger(RemoteAccountService.class)

    RemoteAccountService() {
        super()
    }

    @VisibleForTesting
    RemoteAccountService(URI configFilePath) {
        super(configFilePath)
    }

    /**
     * Register this account
     * @param account
     * @return Customer Number
     */
    public RegisteredAccount createAccount(AccountProfile account) {
        def query = getDefaultQueryParameterMap()
        // customers resource on OCAPI
        def path = "customers"
        client.setAuthenticationToken()

        RegisteredAccount registeredAccount = RegisteredAccount.INVALID

        def registrationObject = [
                "credentials": [
                    "password_question": account.getPassword(),
                    "login": account.getEmail(),
                    "enabled": true,
                    "locked": false
                ],
                "email": account.getEmail(),
                "first_name": account.getFirst(),
                "last_name": account.getLast(),
                "birthday": account.getDateOfBirth(),
                "gender": account.getGender().getValue(),
                "preferred_locale": account.getPreferredLocale(),
        ]

        Map<String, String> data = doPost(path, query, registrationObject)

        if (data.containsKey("customer_no")) {
            AccountProfile profile = new AccountProfile(data.first_name, data.last_name, data.email, null, Gender.getGender(data.gender as String), data.birthday, data.preferred_locale)
            registeredAccount = new RegisteredAccount(data.customer_no, data.email, profile)
        }

        registeredAccount
    }

    public boolean removeAccount(String customerNumber) {
        // TODO: Some sort of protection on customerNumber
        def query = getDefaultQueryParameterMap()

        def path = "customers/${customerNumber}"
        client.setAuthenticationToken()

        doDelete(path, query)
    }

    boolean isActiveCustomerAccount(String customerNumber) {
        boolean found = false

        def query = getDefaultQueryParameterMap()
        def path = "customers/${customerNumber}"
        client.setAuthenticationToken()

        try {
            HttpResponseDecorator resp = client.get(path: path, query: query) as HttpResponseDecorator
            if (resp.getStatus() == HttpStatus.SC_OK) {
                found = true
            }
        } catch (ex) {
            LOG.debug("Customer by customerNumber {} was not found", customerNumber)
        }

        found
    }

    public List<RegisteredAccount> search(String emailAddress) {
        def query = getDefaultQueryParameterMap()
        def path = "customer_search"
        client.setAuthenticationToken()

        def bodyMap = [
                "query": "email = {0}",
                "query_args": [emailAddress],
                "sort_string": "customer_no asc",
                "select": "(**)"
        ]

        Map<String, String> data = doPost(path, query, bodyMap)

        ImmutableList.Builder builder = ImmutableList.builder()

        if (data.size() > 0 && data.count > 0) {
            LOG.debug(data.toString())
            List<Map<String, Object>> hits = data.hits
            for (Map<String, Object> foundCustomer : hits) {

//                foundCustomer.get("customer_no")
//                foundCustomer.get("last_name")
//                foundCustomer.get("birthday")
//                foundCustomer.get("customer_no")
//                foundCustomer.get("email")
//                foundCustomer.get("gender")
//                foundCustomer.get("phone_business")
//                foundCustomer.get("phone_home")
//                foundCustomer.get("phone_mobile")
//                foundCustomer.get("preferred_locale")
//                foundCustomer.get("salutation")

                AccountProfile profile = new AccountProfile(foundCustomer.first_name, foundCustomer.last_name, foundCustomer.email, null, Gender.getGender(foundCustomer.gender as String), foundCustomer.birthday, foundCustomer.preferred_locale)
                RegisteredAccount account = new RegisteredAccount(foundCustomer.customer_no, foundCustomer.email, profile)

                builder.add(account)

            }
        }

        builder.build()
    }
}
