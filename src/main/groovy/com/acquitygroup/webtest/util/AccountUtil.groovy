package com.acquitygroup.webtest.util

import com.acquitygroup.webtest.model.account.Customer
import com.acquitygroup.webtest.page.home.HomePage
import com.acquitygroup.webtest.page.myaccount.LoginPage
import com.acquitygroup.webtest.page.myaccount.LogoutPage
import com.acquitygroup.webtest.page.myaccount.MyAccountPage
import com.acquitygroup.webtest.page.myaccount.RegisterPage
import geb.Browser

class AccountUtil {

    private AccountUtil() {
        throw new IllegalAccessException("This is a utility")
    }

    /**
     * Navigates to the {@link LoginPage} and logs in a customer
     * @param browser {@link geb.Browser}
     * @param customer {@link Customer}
     */
    public static void login(Browser browser, Customer customer) {
        browser.drive {
            toAt(LoginPage)
            loginModule.fillAndSubmitLogin(customer.getEmail(), customer.getPassword())
            at(MyAccountPage)
        }
    }

    /**
     * Uses the Logout endpoint to log out and remove all customer tracking and cart
     * @param browser {@link geb.Browser}
     */
    public static void logout(Browser browser) {
        browser.drive {
            to(LogoutPage)
            at(LoginPage)
        }
    }

    /**
     * Checks to see if a customer is logged in by FirstName&lt;space&gt;LastName.
     *
     * <b>Side Effect NOTE: This directs the page to {@link CartersHomePage}</b>
     *
     * @param browser
     * @param customerName
     * @return
     */
    public static boolean isLoggedIn(Browser browser, String customerName) {
        boolean isLoggedIn = false

        browser.drive {
            // This really sucks, I would rather query the "page" that is currently in scope for a content element, and if that doesn't exist, then goto the home page and attempt to look for the header
            //TODO: find a way to query current page, otherwise do the following
            to(HomePage)
            isLoggedIn = header.loggedInName().text() == customerName
        }

        isLoggedIn
    }

    /**
     * Creates a new account and verifies that the account has been created
     * @param browser {@link geb.Browser}
     * @param localCustomer {@link Customer}
     */
    public static void createAccount(Browser browser, Customer localCustomer) {
        browser.drive {
            given: "go to register page"
            to RegisterPage

            when: "fill form with customer info"
            registerForm.fillForm(localCustomer)

            and: "submit form"
            registerForm.submit.click()

            then: "user is at my account page"
            at MyAccountPage

            and: "verify First Name (firstname)"
            primaryHeader(localCustomer.getFirstName())

            and: "verify header logged in as Customer first, Customer Last"
            header.loggedInName().text() == "${localCustomer.getFirstName()} ${localCustomer.getLastName()}"
        }
    }

}