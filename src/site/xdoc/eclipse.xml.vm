<?xml version="1.0" encoding="UTF-8"?>
<document xmlns="http://maven.apache.org/XDOC/2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/XDOC/2.0 http://maven.apache.org/xsd/xdoc-2.0.xsd">

    <properties>
        <title>Eclipse Setup - Demandware Testing Framework</title>
        <author email="mike.ensor@acquitygroup.com">Mike Ensor</author>
    </properties>

    <!-- Optional HEAD element, which is copied as is into the XHTML <head> element -->
    <head>
        <title>Eclipse Setup - Demandware Testing Framework</title>
        <meta content="Documentation site for Automated Testing Framework" name="description"/>

        #include("header.vm")
    </head>

    <body>
        <a name="back-to-top" id="back-to-top"></a>
        <h1>Eclipse Setup</h1>
        <p>
            This project utilizes Groovy, Maven and Git. Eclipse does not support these technologies natively, but they are supported by community plugins.
        </p>
        <a name="egit" id="egit"/>
        <section name="eGit Plugin Install">
            <p>
                eGit is a plugin for Eclipse that attempts to integrate Git with Eclipse's "Team" SCM functionality. eGit is half-baked and it is recommended
                to use SourceTree, TortoiseGit or the command line to work with git. eGit does provide visual indicators for edited files and that is the main
                reason to install the plugin.
            </p>
            <ol>
                <li>In Eclipse, goto "Help" &gt; "Marketplace" and search for "egit"</li>
                <li>Install egit plugin</li>
                <li>Restart Eclipse</li>
            </ol>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <a name="maven" id="maven"></a>
        <section name="Maven Plugin Install">
            <p>
                The framework is built using Maven 3. Maven is built in to Eclipse Juno but only for the Java version. If you have any other version, please
                follow the steps below to install the
                <em>m2e</em>
                plugin for Maven Eclipse integration.
            </p>
            <ol>
                <li>In Eclipse, goto "Help" &gt; "Marketplace" and search for "m2e"</li>
                <li>Click on "Maven Integration for Eclipse"</li>
                <li>Install m2e plugin</li>
                <li>Restart Eclipse</li>
            </ol>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <a name="groovy" id="groovy"/>
        <section name="Groovy Plugin Install">
            <p>
                Groovy is a JVM based language that compiles down into Java byte-code. In order to do this, the Groovy plugin compiles the Groovy Clases into Java
                classes and places them in the "target" folder. The "target" folder is the default build directory for Maven and the contents are to be considered
                volatile.
            </p>
            <ol>
                <li>In Eclipse, goto "Help" &gt; "Marketplace" and search for "Groovy"</li>
                <li>Install "Groovy Eclipse" plugin</li>
                <li>Restart Eclipse</li>
            </ol>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <section name="Eclipse Project File">
            <a name="eclipse-project-file" id="eclipse-project-file"></a>
            <h3>NOTE: Eclipse Project File Creation</h3>
            <p>
                When first starting or when the Eclipse is acting strange (can't find files, compile errors, etc), the
                <em>.project</em>
                file should be re-generated. Maven can generate the
                <em>.project</em>
                file through the maven-eclipse-plugin. Since this is used often, it is
                recommended to create a "Run Configuration" for easy use.
            </p>
            <ol>
                <li>Setup a Run Configuration to generate the Eclipse Project file
                    <ol>
                        <li>Click on "Run Configurations"</li>
                        <li>Add a "Maven Build" configuration</li>
                        <li>Set the "Base Directory" to the project's base</li>
                        <li>Name the Run Configuration "eclipse:eclipse"</li>
                        <li>Set "Goals" to be "eclipse:eclipse"</li>
                        <li>Click on the "Common" tab</li>
                        <li>Check the "Run" checkbox in "Display in favorites menu"</li>
                        <li>Click on "Run" - This closes and saves the run configuration for easy reuse</li>
                    </ol>
                    <strong>NOTE: Must have m2e installed, see
                        <a href="#maven">Maven Plugin Install</a>
                    </strong>
                </li>
                <li>Right-click on the project in the navigator, select "Refresh"</li>
                <li>Right-click on the "target/generated-sources/groovy-stubs" and select
                    <em>build-path &gt; Exclude from Build Path</em>
                    to remove the duplicate classes in Eclipse.
                    <sup>1</sup>
                </li>
            </ol>
            <p>
                <sup>1</sup>
                - Maven-based polyglot projects in Eclipse have a few bugs, namely the Groovy Stubs are automatically included in the
                <em>BUILD PATH</em>
                and must be ignored.
            </p>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        #include("footer.vm")

    </body>
</document>