SyntaxHighlighter.autoloader(
    'javascript         script/shBrushJScript.js',
    'bash               script/shBrushBash.js',
    'xml                script/shBrushXml.js',
    'groovy             script/shBrushGroovy.js',
    'java               script/shBrushJava.js'
);

alert("Before");
SyntaxHighlighter.all();
alert("after");