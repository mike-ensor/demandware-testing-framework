# Overview
This is a base project for automated testing framework against a Demandware instance.  The technologies used include:

 * Spock - [Spock Documentation][2]
 * Geb - [Geb Documentation][3]
   * Uses WebDriver
      * ChromeDriver [ChromeDriver download][4]
 * Maven 3 - [Maven Download][1]
 * Bamboo - [Bamboo Instance][5] (Please request access from: mike.ensor@acquitygroup.com)
 * SauceLabs - [SauceLabs website][6] (Please request access if you want to see test results including videos)

## Required Environment Variables
saucelabs.username - Username for SauceLabs account (Required for Remote environment)
saucelabs.password - Password/passphrase for SauceLabs account (Required for Remote environment)

# How to Use
Please refer to the [official documentation][7] for information on how to setup development boxes and writing/running tests

# Common Commands
(authentication required for each command)

  * Deploy site documentation to BitBucket site:

        mvn clean site:site scm-publish:publish-scm

  * Deploy snapshot to local repository:

        mvn clean install

  * Run all tests:

        mvn verify


[1]: http://maven.apache.org/download.html "Apache Maven 3"
[2]: https://spock-framework.readthedocs.org/en/latest/ "Spock Documentation"
[3]: http://www.gebish.org/manual/0.9.0-RC-1/all.html "Geb Documentation"
[4]: http://code.google.com/p/chromedriver/downloads/list "ChromeDriver Download List"
[5]: https://mike-acquitygroup.atlassian.net/builds/currentActivity.action "Bamboo Instance"
[6]: https://saucelabs.com/ "SauceLabs"
[7]: http://mikeensor.bitbucket.org/demandware-testing-framework